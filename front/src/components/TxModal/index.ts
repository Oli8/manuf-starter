import type { SvelteComponent } from 'svelte'
import TxModal from './TxModal.svelte'

const TRANSITION_TIME = 235

interface ProgrammaticModal {
  open?: (title: string) => ProgrammaticModalInstance;
  close?: () => void;
}

type ProgrammaticModalComponent = ProgrammaticModal & typeof SvelteComponent
type ProgrammaticModalInstance = ProgrammaticModal & SvelteComponent

function open(title: string) {
  const modal: ProgrammaticModalInstance = new TxModal({
    target: document.body,
    props: { title },
  })
  // Preserve transition
  setTimeout(modal.$set.bind(modal, { active: true }), 1)

  modal.close = () => {
    modal.$set({ active: false })
    setTimeout(modal.$destroy.bind(modal), TRANSITION_TIME)
  }

  return modal
}

(TxModal as ProgrammaticModalComponent).open = open

export default (TxModal as ProgrammaticModalComponent)
