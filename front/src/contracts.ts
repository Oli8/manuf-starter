import type { ContractInterface } from '@ethersproject/contracts'
import type { Address } from './helpers/eth-helper'
import ERC20ABI from '../contracts/erc20.json'

interface ContractJSON {
  abi: ContractInterface;
  networks: {
    [key: string]: {
      address: string;
    }
  }
}

interface ContractData {
  abi: ContractInterface;
  address: Address;
}

function getContractData(data: ContractJSON, networkId=3): ContractData {
  const { abi } = data;
  return {
    abi,
    address: data.networks[String(networkId)].address as Address
  }
}

const ERC20 = {
  abi: ERC20ABI,
  address: '0x123456789'
}

// const MyContract = getContractData(MyContractData, DEV_CHAIN);

export {
  ERC20,
}
