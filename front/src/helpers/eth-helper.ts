import {
  providers,
  Contract,
  type ContractTransaction,
  utils,
  BigNumber,
} from 'ethers';
import { ERC20 } from '../contracts';

export type Address = `0x${string}`;

let readProvider;
if (DEV_CHAIN === 1337) {
  readProvider = new providers.JsonRpcProvider(
    `http://localhost:${DEV_HOST ?? 8525}`
  );
} else if (DEV_CHAIN === 3) {
  readProvider = new providers.InfuraProvider('ropsten');
} else if (isProd) {
  // TODO:
} else {
  readProvider = new providers.InfuraProvider('ropsten');
}
const contract: Contract = new Contract(ERC20.address, ERC20.abi, readProvider);

let provider: providers.Web3Provider;
let contractWithSigner: Contract;
let signer: providers.JsonRpcSigner;

(() => {
	if (!window.ethereum) return;

	provider = new providers.Web3Provider(window.ethereum, 'any');
	signer = provider.getSigner();

	contractWithSigner = new Contract(ERC20.address, ERC20.abi, provider).connect(
		signer
	);
})();

export async function connectWallet(): Promise<Address> {
	try {
		const [account] = await provider.send('eth_requestAccounts', []);
		return account;
	} catch (error) {
		return null;
	}
}

export async function getConnectedWallet(): Promise<Address> {
	try {
		return (await signer.getAddress()) as Address;
	} catch (error) {
		return null;
	}
}

export async function getChain(): Promise<number> {
	try {
		const { chainId } = await provider.getNetwork();
		return chainId;
	} catch (error) {
		return null;
	}
}

export function toWei(amount: number): string {
  // ethers amount to wei in hexadecimal format
  return utils.parseEther(String(amount)).toHexString();
}

export function fromWei(amount: string|BigNumber): number {
  // hexadecimal wei amount to ethers
  return Number(utils.formatEther(amount));
}

// address fetched from contracts are lowercased while those from wallet are not
export function addressMatches(a: Address, b: Address): boolean {
  return a?.toLowerCase() === b?.toLowerCase()
}

export {
  provider,
  contract,
}
