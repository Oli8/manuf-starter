import type { ContractTransaction } from '@ethersproject/contracts'
import type { TransactionReceipt } from '@ethersproject/abstract-provider'
import type { Address } from './eth-helper'
import { Toast } from 'spaper'
import TxModal from '../components/TxModal'
import { provider } from './eth-helper'
import { selectedAccountHasOngoingTx } from '../store'

interface TxError extends Error {
  code?: number|string;
  data?: Object;
  stack?: string;
}

async function doTransaction(
  tx: Function, params: any[] = []
): Promise<[ContractTransaction|TxError, boolean]> {
  try {
    const result: ContractTransaction = await tx.apply(null, params)
    return [result, true]
  } catch(error) {
    return [parseErrorMessage(error), false]
  }
}

interface handleTxWithUxParams {
  title: string
  tx: (...args: any[]) => Promise<ContractTransaction>
  params?: any[]
  messages?: {
    success?: string
    error?: string
  }
  onEnd?: () => any
  onConfirm?: (success: boolean) => any
}

export async function handleTxWithUx({
  title,
  tx,
  params=[],
  messages={},
  onEnd,
  onConfirm
}: handleTxWithUxParams) {
  messages = Object.assign({
    success: 'Transaction confirmée !',
    error: 'La transaction a échouée :(',
  }, messages)
  const txModal = TxModal.open(title)
  const [data, success] = await doTransaction(
    tx,
    params
  );
  selectedAccountHasOngoingTx.set(true)

  if (!success) {
    selectedAccountHasOngoingTx.set(false)
    txModal.close()
    const { code } = data as TxError;
    let errorMessage = 'Une erreur est survenue.';
    if (code === 4001) { // Denied by user
      return;
    }
    if (DEV_CHAIN === 1337) {
      errorMessage += ` ${(data.data as any)
        .message.split(
          'VM Exception while processing transaction: revert '
        )[1]}`
    } else {
      errorMessage += ` ${(data as TxError).message}`
    }

    return Toast.error({
      message: errorMessage,
      indefinite: true,
    })
  }
  const hash = (data as ContractTransaction).hash as Address
  txModal.$set({ txHash: hash })
  provider.once(hash, ({ status }: TransactionReceipt) => {
    const success = status === 1
    const type = success ? 'success' : 'error'
    Toast[type]({
      message: messages[type],
      indefinite: true,
    })
    selectedAccountHasOngoingTx.set(false)
    txModal.close()
    onConfirm?.(success)
  });
  onEnd?.();
}

function parseErrorMessage(error: TxError) {
  const { code, message } = error;
  if (typeof code === 'number')
    return error

  try {
    return JSON.parse(message.substring(
      message.indexOf('{'),
      message.indexOf('}}}') + 3
    ));
  } catch {
    return error;
  }
}
