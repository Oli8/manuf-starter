import type { BigNumber } from 'ethers'

export function formatDate(timestamp: string|BigNumber): string {
  const date: Date = new Date(Number(timestamp) * 1000);
  return [
    date.getDate(), date.getMonth()+1, date.getFullYear()
  ].join('/');
}
