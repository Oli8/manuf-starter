import type { Network } from '@ethersproject/networks'
import { type Readable, type Writable,
         derived,  writable } from 'svelte/store'
import type { Address } from './helpers/eth-helper'
import {
  getConnectedWallet,
  getChain,
  provider,
} from './helpers/eth-helper'

export const selectedAccount: Writable<Address> = writable(null);
export const chainId: Writable<number> = writable(null);
export const hasWallet: Writable<boolean> = writable(true);
export const selectedAccountHasOngoingTx: Writable<boolean> = writable(false);

const SUPPORTED_NETWORK = [3, 1337]

export const connected: Readable<boolean> = derived(
	selectedAccount,
	($selectedAccount) => !!$selectedAccount
);
export const onSupportedNetwork: Readable<boolean> = derived(
	chainId,
	($chainId) => SUPPORTED_NETWORK.includes($chainId)
);

(async () => {
	if (!window.ethereum) {
		hasWallet.set(false);
		return;
	}

	// Can't be done through provider as far as I know
	window.ethereum.on('accountsChanged', ([account]) => {
		selectedAccount.set(account);
		window.location.reload();
	});
	provider.on('network', (_network: Network, previousNetwork: Network) => {
		if (previousNetwork) {
			window.location.reload();
		}
	});

  const connectedAddress = await getConnectedWallet();
	selectedAccount.set(connectedAddress);
	chainId.set(await getChain());
})();
