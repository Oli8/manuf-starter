Requires a `.env` file with:

API_URL="..." // Provider URI (Infura...)

PRIVATE_KEY="..." // Wallet private key or check truffle config file for other options like mnemonic

Requires truffle

`npm i -g truffle`

Install dependencies:

`npm i`

### Commands

Compile Contracts

`truffle compile`

Deploy (dev):

`truffle deploy`

Run specific migration (Useful to deploy only one contract by using same index)

`truffle migrate --f $start_index --to $end_index`

Deploy (ropsten):

`truffle deploy --network ropsten`

Open console:

`truffle console`
`truffle console --network ropsten`

You can then access your contracts as such:
```javascript
const contract = await YourContract.deployed()
await contract.someFunction()
```
