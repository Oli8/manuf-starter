// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "./interfaces/IHackPot.sol";

contract Hackpot is IHackPot {
    function create(string calldata title) external override {}

    function pot(uint256 id) external view override returns (Pot memory) {}

    function donate(uint256 id) external payable override {}

    function withdraw(uint256 id) external override {}

    function offer(uint256 id, address beneficiary) external override {}

    function _transferEth(address to, uint256 amount) private {
        (bool sent,) = to.call{value: amount}("");
        require(sent, "Transfer failed");
    }
}
