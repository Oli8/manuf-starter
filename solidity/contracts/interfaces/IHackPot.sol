// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

struct Pot {
    uint256 id;
    uint256 value;
    string title;
    bool active;
    address owner;
}

interface IHackPot {
    event PotOpen(address from, string title);

    event Donation(address from, uint256 id, uint256 amount);

    event Withdraw(address from, uint256 id, uint256 amount);

    event Offer(address from, address to, uint256 id, uint256 amount);

    // Create a new pot
    function create(string calldata title) external;

    // Retrieve pot informations
    function pot(uint256 id) external view returns (Pot memory);

    // Donate to a pot
    function donate(uint256 id) external payable;

    // Withdraw pot amount to owner
    function withdraw(uint256 id) external;

    // Send pot amount
    function offer(uint256 id, address beneficiary) external;
}
