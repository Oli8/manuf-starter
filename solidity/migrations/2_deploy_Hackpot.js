const Hackpot = artifacts.require("Hackpot");

module.exports = function (deployer, network, [alice]) {
  if (network === 'development') {
    return deployer.deploy(
      Hackpot,
      { from: alice }
    )
  }

  return deployer.deploy(Hackpot)
};
