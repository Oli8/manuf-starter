const {
  expectRevert,
  expectEvent,
  balance,
} = require('@openzeppelin/test-helpers')
const {
  expectPass,
  from,
  ethToWei,
  toBN,
} = require('./helpers/utils')
const { matchPot } = require('./helpers/pot')

const Hackpot = artifacts.require('Hackpot')

const POT_TITLE = "Financer les manufs d'Olivier <3"
const AMOUNT_DONATED = ethToWei('0.1')

contract('Hackpot', ([alice, bob]) => {
  let contract, creation

  beforeEach(async () => {
    contract = await Hackpot.new()
    creation = await contract.create(POT_TITLE, from(alice))
  })

  xcontext('creation', async () => {
    it('should be possible to create new pot', async () => {
      expectPass(creation)
    })

    it('it should emit event', async () => {
      expectEvent(
        creation, 'PotOpen',
        {
          from: alice,
          title: POT_TITLE
        }
      )
    })

    it('should list new pot', async () => {
      const pot = await contract.pot(1)
      expect(matchPot(pot, {
        title: POT_TITLE,
        owner: alice,
        active: true,
        value: '0',
      })).to.be.ok
    })
  })

  xcontext('donate', async () => {
    let donation
    beforeEach(async () => {
      donation = await contract.donate(
        1, { from: bob, value: AMOUNT_DONATED }
      )
    })

    it('should increase pot value', async () => {
      const { value } = await contract.pot(1)
      expect(value).to.eq(AMOUNT_DONATED)
    })

    it('should emit event', async () => {
      expectEvent(
        donation, 'Donation',
        {
          from: bob,
          id: toBN(1),
          amount: AMOUNT_DONATED
        }
      )
    })

    it("It should revert if pot doesn't exist", async () => {
      await expectRevert(
        contract.donate(404, { from: bob, value: AMOUNT_DONATED }),
        'Pot does not exist'
      )
    })
  })

  xcontext('withdraw', async () => {
    context('valid', async () => {
      let withdrawal, aliceBalanceBefore
      beforeEach(async () => {
        aliceBalanceBefore = balance.current(alice)
        await contract.donate(
          1, { from: bob, value: AMOUNT_DONATED }
        )
        withdrawal = await contract.withdraw(1, from(alice))
      })

      it('should allow owner to withdraw', async () => {
        expectPass(withdrawal)
      })

      it('should emit event', async () => {
        expectEvent(
          withdrawal, 'Withdraw',
          {
            from: alice,
            id: toBN(1),
            amount: AMOUNT_DONATED,
          }
        )
      })

      it('should close pot', async () => {
        const { active } = await contract.pot(1)
        expect(active).to.be.false
      })

      it('should send amount to withdrawer', async () => {
        const balanceAfter = await balance.current(alice)
        expect(balanceAfter.gt(aliceBalanceBefore)).to.be.true
      })
    })

    it('should fail if outsider tries to withdraw', async () => {
      await contract.donate(
        1, { from: bob, value: AMOUNT_DONATED }
      )
      await expectRevert(
        contract.withdraw(1, from(bob)),
        'Forbidden: Only pot owner can withdraw'
      )
    })

    it("It should revert if pot doesn't exist", async () => {
      await expectRevert(
        contract.withdraw(404, from(alice)),
        'Pot does not exist'
      )
    })
  })

  xcontext('offer', async () => {
    context('valid', async () => {
      let offering, bobBalanceBefore
      beforeEach(async () => {
        bobBalanceBefore = balance.current(bob)
        await contract.donate(
          1, { from: alice, value: AMOUNT_DONATED }
        )
        offering = await contract.offer(1, bob, from(alice))
      })

      it('should allow owner to offer pot', async () => {
        expectPass(offering)
      })

      it('should emit event', async () => {
        expectEvent(
          offering, 'Offer',
          {
            from: alice,
            to: bob,
            id: toBN(1),
            amount: AMOUNT_DONATED,
          }
        )
      })

      it('should close pot', async () => {
        const { active } = await contract.pot(1)
        expect(active).to.be.false
      })

      it('should send amount to beneficiary', async () => {
        const balanceAfter = await balance.current(bob)
        expect(balanceAfter.gt(bobBalanceBefore)).to.be.true
      })
    })

    it('should fail if outsider tries to offer', async () => {
      await contract.donate(
        1, { from: bob, value: AMOUNT_DONATED }
      )
      await expectRevert(
        contract.offer(1, alice, from(bob)),
        'Forbidden: Only pot owner can offer'
      )
    })

    it("It should revert if pot doesn't exist", async () => {
      await expectRevert(
        contract.offer(404, alice, from(bob)),
        'Pot does not exist'
      )
    })
  })
})
