function matchPot(pot, { title, owner, active, value }) {
  return title === pot.title
    && owner === pot.owner
    && active === pot.active
    && value === pot.value
}

module.exports = {
  matchPot,
}
