const { web3 } = require('@openzeppelin/test-helpers/src/setup')
const { toWei, toBN } = web3.utils

const ETHER_UNIT = 'ether'

function expectPass(tx) {
  expect(tx.receipt.status).to.be.true
}

function from(address) {
  return { from: address }
}

function ethToWei(amount) {
  return toWei(amount, ETHER_UNIT)
}

module.exports = {
  expectPass,
  from,
  ethToWei,
  toBN,
}
